import { BefortestPage } from './app.po';

describe('befortest App', function() {
  let page: BefortestPage;

  beforeEach(() => {
    page = new BefortestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
