import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;
  deletePost(post){
    this.posts.splice(this.posts.indexOf(post),1)
  }

  constructor(private _postService:PostsService) { }

  ngOnInit() {
    //this.posts = this._postService.getPosts();
    this._postService.getPosts().subscribe(postsData=>this.posts=postsData) 
  }

}
