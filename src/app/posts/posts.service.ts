import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {
  private _url = 'https://jsonplaceholder.typicode.com/posts';

  getPosts(){
   /* let posts= [
      {title:'title1', content:'1111', author:'aaaaa'},
      {title:'title2', content:'2222', author:'bbbb'},
      {title:'title3', content:'3333', author:'cccc'},
      {title:'title4', content:'4444', author:'dddd'},

    ]
    return posts;*/
    return this._http.get(this._url).map(res=>res.json())
  }

  constructor(private _http:Http) { }

}
