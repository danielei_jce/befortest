import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs: ['post']
})
export class PostComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<Post>();
  post:Post;

  sendDelete(){
    this.deleteEvent.emit(this.post)
  }

  constructor() { }

  ngOnInit() {
  }

}
