import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`.users li { cursor: default; }
               .users li:hover { background: #ecf0f1; } 
               .list-group-item.active,
               .list-group-item.active:hover{
                background-color: #ecf0f1;
                border-color: #ecf0f1; 
                color: #2c3e50; }
  `]
})
export class UsersComponent implements OnInit {
  currentUser;
  users;
  isLoading:boolean = true;

  select(user){
    this.currentUser=user;
  }
  deleteUser(user){
    this.users.splice(this.users.indexOf(user),1)
  }
  addUser(user){
    this.users.push(user);
  }
  constructor(private _usersService:UsersService) { }

  ngOnInit() {
  //this.users = this._usersService.getUsers();
  this._usersService.getUsers().subscribe(usersData=>this.users=usersData)
  }

}
