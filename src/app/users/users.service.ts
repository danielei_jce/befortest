import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsersService {
 
 private _url = 'https://jsonplaceholder.typicode.com/users';

  getUsers(){

  /*let users = [
    {name:'daniel', email:'111@11'},
    {name:'naama', email:'222@11'},
    {name:'tal', email:'333@11'},
  ]
  return users;*/
  return this._http.get(this._url).map(res=>res.json());
  }

  constructor(private _http:Http) { }

}
