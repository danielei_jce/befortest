import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from './user';


@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {
  @Output() deleteEvent = new EventEmitter<User>();
  user:User;
  isEdit:Boolean = false;
  editButtonText = 'Edit';
  sendDelete(){
    this.deleteEvent.emit(this.user)
  }
  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit? this.editButtonText= 'Save' : this.editButtonText= 'Edit';
  }
  constructor() { }

  ngOnInit() {
  }

}
